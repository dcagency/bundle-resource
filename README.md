Bundle Resource
===============

Bundle Resource is a simple module that provides separate Service 
resource entry for each node and taxonomy bundles defined in the system. 
The module is extendible, so it's easy to provide bundle resources for 
other entities using hook_bundle_resources().