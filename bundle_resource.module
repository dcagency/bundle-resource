<?php
/**
 * @file
 * Main module file for Bundle Resource.
 */

/**
 * Implements hook_hook_info().
 */
function bundle_resource_hook_info() {
  $hooks['bundle_resources'] = array(
    'group' => 'bundle_resource',
  );
  return $hooks;
}


/**
 * Returns information about resource API version information.
 * The resource API is the way modules expose resources to services,
 * not the API that is exposed to the consumers of your services.
 *
 * @return array
 *  API version information. 'default_version' is the version that's assumed
 *  if the module doesn't declare an API version. 'versions' is an array
 *  containing the known API versions. 'current_version' is the current
 *  version number.
 */
function bundle_resource_resource_api_version_info() {
  $info = array(
    'default_version' => 3001,
    'versions' => array(3002),
  );
  $info['current_version'] = max($info['versions']);
  return $info;
}

/**
 * Implements hook_services_resources().
 */
function bundle_resource_services_resources() {
  // Fetch bundle resources registered by external modules
  $resources = module_invoke_all('bundle_resources');

  // Build resource definition array for each registered bundle
  foreach ($resources as $entity_name => $entity_settings) {
    foreach ($entity_settings['bundles'] as $bundle_name => $bundle_settings) {
      $type_resource_definition[$bundle_name] = $entity_settings['entity_resource_definition'];
    }
  }

  return $type_resource_definition;
}

/**
 * Implementation of hook_bundle_resources
 *
 * @return mixed
 */
function bundle_resource_bundle_resources() {
  // Node bundles
  module_load_include('inc', 'services', 'resources/node_resource');

  // Get all bundles of node and prepare bundles array
  $types = node_type_get_types();
  $node_bundles = array();
  foreach($types as $type_name => $type_definition) {
    $node_bundles[$type_name] = $type_name;
  }

  // Retrieve default node resource definition.
  // It will be used as base for all node bundle resources.
  $node_resource_definition = _node_resource_definition();

  // Build bundle resources definition for node entity.
  $bundle_resources['node'] = array(
    'entity_resource_definition' => $node_resource_definition['node'],
    'bundles' => $node_bundles
  );

  // Taxonomy bundles
  module_load_include('inc', 'services', 'resources/taxonomy_resource');

  // Get all bundles associated with taxonomy term (vocabularies)
  $vocabularies = taxonomy_get_vocabularies();
  $taxonomy_bundles = array();
  foreach($vocabularies as $vocabulary) {
    $taxonomy_bundles[$vocabulary->machine_name] = $vocabulary->machine_name;
  }

  // Retrieve default taxonomy resource definition.
  // It will be used as base for all taxonomy bundle resources.
  $taxonomy_resource_definition = _taxonomy_resource_definition();

  // Build bundle resources definition for node entity.
  $bundle_resources['taxonomy_term'] = array(
    'entity_resource_definition' => $taxonomy_resource_definition['taxonomy_term'],
    'bundles' => $taxonomy_bundles
  );

  return $bundle_resources;
}